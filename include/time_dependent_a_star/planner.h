/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef TIME_DEPENDENT_PLANNER
#define TIME_DEPENDENT_PLANNER

#include <memory>
#include "lemon/smart_graph.h"
#include <lemon/concepts/digraph.h>
#include <lemon/concepts/graph.h>
#include "lemon/maps.h"

#include <lemon/adaptors.h>
#include "lemon/bfs.h"  // Used by GetHeuristicCost
#include "lemon/dijkstra.h"  // Used by GetHeuristicCost
#include <dual_manipulation_planner/graph_creator.h>

typedef u_int32_t node_id;
typedef u_int32_t time_m;
typedef time_m cost;

#define MAX_TIME 300
#define UNFEASIBLE_PATH_COST 10000000
#define HEURISTIC_COST 15

class Node
{
public:
    node_id id;
    time_m start_time, end_time;
    node_id father_id;
    endeffector_id ee_id;
    workspace_id ws_id;
    cost father_cost=0;
    Node(){};
    
    ///
    Node(node_id id_, time_m start_time_, node_id father_id_)
    {
      id = id_;
      start_time = start_time_;
      father_id = father_id_;
    }
    
    ///
    Node(node_id id_, time_m start_time_)
    {
      id = id_;
      start_time = start_time_;
      father_id = -1;
    }
};

class Plan
{
public:
    std::vector<Node> nodes;
    uint16_t priority;
    ///
    Plan() : nodes() {}
    
    ///
    Plan(Node node_) : nodes(1,node_) {}
    
    ///
    void Back(Node node_)
    {
      nodes.push_back(node_);
    }
    
    ///
    void Front(Node node_)
    {
      nodes.insert( nodes.begin(), node_ );
    }
};

class DynamicArc
{
public:
    std::vector<cost> cost_map; //The vector goes from 0 to max_time, I don't care about RAM because the DynamicArc map is sparse
};

typedef lemon::SmartDigraph Graph;
typedef lemon::FilterArcs<Graph> FilterGraph;
typedef lemon::ReverseDigraph<Graph> ReverseGraph;

typedef Graph::ArcMap<cost> ArcCostMap;
typedef ReverseGraph::NodeMap<cost> NodeCostMap;
typedef lemon::Dijkstra<ReverseGraph, ArcCostMap> HeuristicAlg;
typedef Graph::ArcMap<bool> FilterMap;

class Planner
{
public:
  Graph& m_graph;
  ReverseGraph m_rev_graph;
  ArcCostMap static_arc_map;
  std::map<int,DynamicArc> dynamic_arc_map;

  HeuristicAlg heuristic_alg;
  NodeCostMap dist_map;
  uint16_t priority;
  
  FilterMap filter;
  
public:
  Planner(Graph& graph);
  Plan addObject(node_id source_, node_id target_, time_m t0_, uint16_t priority_);
  void addFilteredArc(Graph::Arc filtered_arc);
  void addLocking(const std::vector<Plan>& plans);
  void addLocking(const Plan& plan);
  void clearLocking();
  void initFakeStaticMap();
  void setStaticMap(const ArcCostMap& map);
  virtual std::vector<int> getRelatedNodes(int node_id,int ws_id, int priority);
  virtual endeffector_id getEndEffectorFromNode(node_id id) const;
  virtual workspace_id getWsFromNode(node_id id) const;
  static Plan unify(Plan a, Plan b);
  void clearFilteredArcs();
  ~Planner(){}
private:
  
protected:
    /**
     * @brief Get an estimated cost from node test_ to node target_
     * 
     * @param test_ source node
     * @param target_ target node
     * @return cost
     */    
  cost GetHeuristicCost(Graph::Node & test_, Graph::Node & target_);

  /**
   * @brief Get the time varying cost of arc out_arc_ when the starting time is time_
   * 
   * @param out_arc_ arc to be processed
   * @param time_ starting time of the arc
   * @return cost
   */
  cost GetArcCost(lemon::SmartDigraphBase::Arc& out_arc_, time_m time_);
};


#endif