/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "time_dependent_a_star/planner.h"
#include <dual_manipulation_planner/planner_lib.h>
#include <gmlreader/gmlreader.hpp>
#include <lemon/smart_graph.h>
#include <gmlreader/lemon_wrapper.hpp>

#include <ros/ros.h>
#include <ros/package.h>
#include <thread>

#include <vector>

void printPath(std::vector<dual_manipulation_shared::planner_item>& path)
{
    std::ostringstream os;
    os<<std::endl<<std::endl;
    for(auto p:path)
    {
        os << p.workspace_id << " " << p.grasp_id << " " << p.departure_time << std::endl;
    }
    std::cout<<os.str();
    std::cout.flush();
}

void call_remote_plan(dual_manipulation::planner::planner_lib* pl,grasp_id source_grasp_id, workspace_id source_workspace_id, grasp_id target_grasp_id, workspace_id target_workspace_id)
{
    std::vector<dual_manipulation_shared::planner_item> path;
    if(pl->remote_plan(source_grasp_id, source_workspace_id, target_grasp_id, target_workspace_id,path))
    {
        std::cout <<pl->getName()<< " plan from " << source_grasp_id << " in " << source_workspace_id << " to " << target_grasp_id << " in " << target_workspace_id << " worked!" << std::endl;
        printPath(path);
        return;
//         sleep(1);
        if(pl->getName()=="Cylinder")
        {
            std::cout<<"Faking a conversion failure with Cylinder"<<std::endl;
            pl->add_filtered_arc(11,3,16,3);
            pl->add_filtered_arc(3,3,6,3);
            if(pl->remote_plan(source_grasp_id, source_workspace_id, target_grasp_id, target_workspace_id,path))
            {
                std::cout <<pl->getName()<< " plan from " << source_grasp_id << " in " << source_workspace_id << " to " << target_grasp_id << " in " << target_workspace_id << " worked!" << std::endl;
                printPath(path);
                if(pl->barrier())
                    std::cout <<pl->getName()<< " plan accepted!" << std::endl;
                else
                    std::cout <<pl->getName()<< " plan rejected!" << std::endl;
            }
        }
        else
        {
            if(pl->barrier())
                std::cout <<pl->getName()<< " plan accepted!" << std::endl;
            else
            {
                std::cout <<pl->getName()<< " plan rejected!" << std::endl;
                pl->add_filtered_arc(3,3,6,3);
                std::cout<<"Adding filtered arc 3 3 - 6 3"<<std::endl;
                if(pl->remote_plan(source_grasp_id, source_workspace_id, target_grasp_id, target_workspace_id,path))
                {
                    std::cout <<pl->getName()<< " plan from " << source_grasp_id << " in " << source_workspace_id << " to " << target_grasp_id << " in " << target_workspace_id << " worked!" << std::endl;
                    printPath(path);
                    if(pl->barrier())
                        std::cout <<pl->getName()<< " plan accepted!" << std::endl;
                    else
                        std::cout <<pl->getName()<< " plan rejected!" << std::endl;
                }
            }
        }
    }
    else
    {
        std::cout <<pl->getName()<< " plan from " << source_grasp_id << " in " << source_workspace_id << " to " << target_grasp_id << " in " << target_workspace_id << " DID NOT work!" << std::endl;
    }
}

int main(int argc, char** argv)
{
    ros::init(argc,argv,"pippo");
// // //     graph_publisher=node.advertise<dual_manipulation_shared::graph>("computed_graph",100,this);
    ros::AsyncSpinner spinner(2);
    spinner.start();
    dual_manipulation::planner::planner_lib plannerLib1,plannerLib2, plannerLib3;
// // //     std::vector<dual_manipulation_shared::planner_item> path1,path2;
    sleep(1);
    
    // when set_object is called, a graph is published on "computed_graph" topic
    // the desired behaviour can maybe be achieved interleaving set_object and draw_path (on object change)
    plannerLib1.set_object(51, "Cylinder51",51);
//     plannerLib1.add_filtered_arc(7,1,5,1);
    
    plannerLib2.set_object(52, "Cylinder52",52);
//     plannerLib2.add_filtered_arc(18,2,14,2);
    
    plannerLib3.set_object(53, "Cylinder53",53);
//     plannerLib2.add_filtered_arc(18,2,14,2);
    
    std::thread th1(call_remote_plan,&plannerLib1,51407, 5, 51403, 1);
    std::thread th2(call_remote_plan,&plannerLib2,52407, 5, 52403, 1);
    std::thread th3(call_remote_plan,&plannerLib3,53407, 5, 53403, 1);
    
    th1.join();
    th2.join();
    th3.join();
// // //     bool res,res1,res2;
//     res = plan(grasp_id source_grasp_id, workspace_id source_workspace_id, grasp_id target_grasp_id, workspace_id target_workspace_id, std::vector< dual_manipulation_shared::planner_item >& path);
    
    //     bool plan(grasp_id source_grasp_id, workspace_id source_workspace_id, grasp_id target_grasp_id, workspace_id target_workspace_id, std::vector< dual_manipulation_shared::planner_item >& path);
    //     bool remote_plan(grasp_id source_grasp_id, workspace_id source_workspace_id, grasp_id target_grasp_id, workspace_id target_workspace_id, std::vector< dual_manipulation_shared::planner_item >& path);
// // //     res1 = plannerLib1.remote_plan(7, 1, 7, 5, path1);
// // //     res2 = plannerLib2.remote_plan(18, 2, 18, 4, path2);
    
// // //     res1 = plannerLib1.barrier();
// // //     res2 = plannerLib2.barrier();

//     plannerLib1.draw_path();
//     plannerLib2.draw_path();
    
// // //     printPath(path1);
// // //     printPath(path2);

    return 0;
}