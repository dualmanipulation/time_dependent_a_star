/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "time_dependent_a_star/planner.h"
#include <gmlreader/gmlreader.hpp>
#include <lemon/smart_graph.h>
#include <gmlreader/lemon_wrapper.hpp>

#include <ros/ros.h>
#include <ros/package.h>

#include <vector>

void printPlan(Plan& p)
{
    for(Node n:p.nodes)
    {
        std::cout<<n.id<<" "<<n.start_time<<std::endl;
    }
}

int main(int argc, char** argv)
{
  // Lanciare eseguibile da time_dependent_a_star:
    std::string path=ros::package::getPath("time_dependent_a_star");
    std::ifstream file(path+"/test/test3.gml");
    if(!file.is_open())
    {
      ROS_ERROR("Unable to open graph file!");
      return -1;
    }
    
    lemon::SmartDigraph graph;
    lemon_gml reader;
    reader.read(file,graph);
    
    Planner planner(graph);
    planner.initFakeStaticMap();
    
//     std::vector<Plan> l_locking;
    Plan union_plan;
    Plan temp = planner.addObject(1,8,0, 1);
    std::cout<<"plan from 1 to 8"<<std::endl;
    printPlan(temp);
//     l_locking.push_back(temp);
    union_plan=Planner::unify(union_plan,temp);
    planner.priority=2;
    planner.priority = 2;
    planner.addLocking(union_plan);
    std::cout<<"plan from 1 to 8"<<std::endl;
    temp=planner.addObject(1,8,0, 2);
    printPlan(temp);
    union_plan=Planner::unify(union_plan,temp);
//     l_locking.push_back(temp);
    
    planner.priority = 3;
    planner.addLocking(union_plan);
    std::cout<<"plan from 1 to 8"<<std::endl;
    temp=planner.addObject(1,8,0,3);
    printPlan(temp);
    return 0;
}