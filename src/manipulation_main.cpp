/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "time_dependent_a_star/planner.h"
#include <time_dependent_a_star/manipulation_planner.h>
#include <gmlreader/gmlreader.hpp>
#include <lemon/smart_graph.h>
#include <gmlreader/lemon_wrapper.hpp>
#include <dual_manipulation_shared/planner_service.h>
#include <dual_manipulation_shared/planner_service_response.h>
#include <dual_manipulation_shared/planner_service_request.h>
#include <ros/ros.h>
#include <std_msgs/Header.h>

#include <vector>

// object ids AND their priorities: keep them sorted!!!
std::vector<int> obj_ids({51,52,53});

class multiobject_planner
{
private:
    
    //Status of the system
    std::vector<manipulation_planner*> planners; //vector of planners*
    std::map<int,const manipulation_planner*> planner_map;//From priority to planner*
    std::map<int,object_id> priority_to_object_id;//From priority to object_id
    std::map<int,bool> barrier_requested; //From priority to bool
    std::map<int,dual_manipulation_shared::planner_service_request> requests;//From priority to the request to plan
    
    //Ros stuff to get and send plans and synchronization
    ros::NodeHandle nh;
    ros::Subscriber plan_request_sub;
    ros::Publisher plan_pub;
    ros::Subscriber multiplan_barrier_sub;
    ros::Publisher multiplan_barrier_pub;

    std::vector<Plan> plans;//This is only used for addLocking
    std::vector<int> priorities;
    std::vector<dual_manipulation_shared::planner_service_response::_path_type> paths; //This is where the real plans are stored
    
    //Just save the request, check if all objects are ready, and get ready to multiplan!
    void plan_request_callback(const dual_manipulation_shared::planner_service_request::ConstPtr& msg)
    {
        std::cout << __func__ << " : " << *msg << std::endl;
        requests[msg->priority]=*msg;
        if(requests.size()==num_objects)
            multiplan=true;
        if (barrier)
        {
            std::cout<<barrier_requested.size()<<" "<<num_objects<<std::endl;
            if (barrier_requested.size()==num_objects)
            {
                publish_barrier(true);
            }
            else if (barrier_requested.size()+requests.size()==num_objects)
            {
                publish_barrier(false);
            }
        }
    }
    
    //Either be happy because everybody requested a barrier, be sad because somebody requested a replan, or wait for everybody to do something
    void multiplan_barrier_callback(const std_msgs::HeaderConstPtr& msg)
    {
        std::cout << __func__ << " : " << *msg << std::endl;
        barrier_requested[msg->seq]=true;//Use seq as priority
        std::cout<<barrier_requested.size()<<" "<<num_objects<<std::endl;
        if (barrier_requested.size()==num_objects)
        {
            publish_barrier(true);
        }
        else if (barrier_requested.size()+requests.size()==num_objects)
        {
            publish_barrier(false);
        }
        else
        {
            return;
        }
    }
    
    void publish_plan(int i)
    {
        dual_manipulation_shared::planner_service_response resp;
        resp.ack=paths[i].size();
        resp.path=paths[i];
        resp.status=paths[i].size()?"planned":"failed";
        resp.priority=priorities[i];
        plan_pub.publish(resp);
    }
    
    void publish_barrier(bool ok)
    {
        std::cout<<"publishing barrier response"<<std::endl;
        std_msgs::Header msg;
        msg.frame_id=ok?"go":"replan";
        msg.stamp=ros::Time::now();
        msg.seq=ok;
        barrier=false;
        multiplan=false;
        multiplan_barrier_pub.publish(msg);
        union_plan.nodes.clear();
    }
    
public:    
    multiobject_planner(int num_objects)
    {
        this->num_objects=num_objects;
        barrier=false;
        multiplan=false;
        multiplan_barrier_pub=nh.advertise<std_msgs::Header>("/barrier_result",10);
        plan_pub=nh.advertise<dual_manipulation_shared::planner_service_response>("/remoteplan",10);
        plan_request_sub = nh.subscribe<dual_manipulation_shared::planner_service_request>("/requestplan",10,&multiobject_planner::plan_request_callback,this);
        multiplan_barrier_sub = nh.subscribe<std_msgs::Header>("/barrier_request",10,&multiobject_planner::multiplan_barrier_callback,this);
    }
    
    
    void addFakePlanners()
    {
        std::vector<int> priorities(obj_ids);
        
        for(int i=0; i<obj_ids.size(); i++)
        {
            manipulation_planner* planner=new manipulation_planner();
            int object_id = obj_ids.at(i);
            int priority = priorities.at(i);
            planner->setObject(object_id,"obj" + std::to_string(object_id),priority);
            planners.push_back(planner);
            planner_map[priority]=planner;
            planner->initFakeStaticMap();
            priority_to_object_id[priority]=object_id;
        }
        
        for (auto planner:planners)
        {
            planner->setEndEffectorMap(planner_map);//TODO this should be done when all the objects are known and all the planners have been created
        }
    }
    
    void run()
    {
//         static int counter = 0;
//         if (counter++ % 10)
//             std::cout<<"running"<<std::endl;
        for (manipulation_planner* planner:planners)//TODO ensure this is ordered by priority
        {
//             planner->graph_publisher.publish(planner->message);
        }
        if (!multiplan) return;
//         plans.clear();
        paths.clear();
        priorities.clear();
        barrier_requested.clear();
        for (manipulation_planner* planner:planners)//TODO ensure this is ordered by priority
        {
            dual_manipulation_shared::planner_service_response::_path_type path;
            planner->clearLocking();
//             planner->graph_publisher.publish(planner->message);
            if(!union_plan.nodes.empty())
                planner->addLocking(union_plan);
            auto a = requests.at(planner->priority).source.grasp_id;
            auto b = requests.at(planner->priority).source.workspace_id;
            auto c = requests.at(planner->priority).destination.grasp_id;
            auto d = requests.at(planner->priority).destination.workspace_id;
            planner->clearFilteredArcs();
            for (int i=0;i<requests.at(planner->priority).filtered_source_nodes.size();i++)
            {
                std::cout<<"filtering "<<requests.at(planner->priority).filtered_source_nodes[i].grasp_id<<" "<<requests.at(planner->priority).filtered_target_nodes[i].grasp_id<<std::endl;
                planner->addFilteredArc(requests.at(planner->priority).filtered_source_nodes[i],requests.at(planner->priority).filtered_target_nodes[i]);
            }
            
            auto last_plan=planner->plan(a,b,c,d,path);
            union_plan=Planner::unify(union_plan, last_plan);
//             plans.clear();
//             plans.push_back(union_plan);
            paths.push_back(path);
            priorities.push_back(planner->priority);
            planner->priority;
            
        }
        multiplan=false;
        requests.clear();
        barrier=true;
        std::cout<<"publishing plans"<<std::endl;
        for (int i=0;i<paths.size();i++)
        {
            usleep(1000);
            publish_plan(i);
        }
    }
    private:
    int num_objects;
    bool multiplan;
    bool barrier;
    Plan union_plan;
};


int main(int argc, char** argv)
{
    ros::init(argc,argv,"multiobject_planner");
    ros::NodeHandle nh;
    multiobject_planner planner(obj_ids.size());
    sleep(1);
    planner.addFakePlanners();
    ros::Rate rate(10);
    while(ros::ok())
    {
//         rate.sleep();
        usleep(600000);
        planner.run();
        ros::spinOnce();
    }
    return 0;
}