/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "time_dependent_a_star/manipulation_planner.h"

manipulation_planner::manipulation_planner():graphCreator(),Planner(graph)
{
    
}

bool manipulation_planner::setObject(object_id id, std::string name, int priority)
{
    std::cout<<"object set to "<<id<<" with name "<<name<<std::endl;
    obj.id=id;
    create_graph(obj);
    this->priority=priority;
    return true;
}

endeffector_id manipulation_planner::getEndEffectorFromNode(node_id id) const
{
    endeffector_id ee;
    workspace_id ws;
    grasp_id gr;
    getInfoFromNode(id,ee,gr,ws);
    return ee;
}

workspace_id manipulation_planner::getWsFromNode(node_id id) const
{
    endeffector_id ee;
    workspace_id ws;
    grasp_id gr;
    getInfoFromNode(id,ee,gr,ws);
    if(!graphCreator::isEndEffectorMovable(ee))
    {
        return ws;
    }
    return UNFEASIBLE_PATH_COST;
}

void manipulation_planner::getInfoFromNode(node_id id, endeffector_id& ee_id, grasp_id& grasp, workspace_id& workspace) const
{
    getNodeInfo(graph.nodeFromId(id),grasp,workspace);
    ee_id = std::get<1>(database.Grasps.at(grasp));
}

void manipulation_planner::setEndEffectorMap(const std::map< int, const manipulation_planner* >& eeMap)
{
    this->eeMap=eeMap;
}


std::vector< int > manipulation_planner::getRelatedNodes(int ee_id,int ws_id,int priority)
{
    std::vector<int> result;
    auto ee = graph_as_map[ee_id];
    static std::map<int,bool> ee_ids;
    if(!ee_ids.count(ee_id))
    {
        ee_ids[ee_id] = true;
        std::cout << "ee_id: " << ee_id << " > is " << (isEndEffectorMovable(ee_id)?"":"NOT ") << "movable!!!" << std::endl;
    }
    
    if(!isEndEffectorMovable(ee_id))
    {
        for(auto grasp: ee[ws_id])
        {
            result.push_back(grasp.second);
        }
    }
    else
    {
        for (auto workspace: ee)
        {
            for (auto grasp: workspace.second)
            {
                result.push_back(grasp.second);
            }
        }
    }
    return result;
}

void manipulation_planner::addFilteredArc(dual_manipulation_shared::planner_item source, dual_manipulation_shared::planner_item destination)
{
    auto a = source.grasp_id;
    auto b = source.workspace_id;
    auto c = destination.grasp_id;
    auto d = destination.workspace_id;
    lemon::SmartDigraph::Arc arc;
    if(getArc(a,b,c,d,arc))
        Planner::addFilteredArc(arc);
}

std::string manipulation_planner::getName()
{
    return "TODO implement getName";
}

Plan manipulation_planner::plan(grasp_id source_grasp_id, workspace_id source_workspace_id, grasp_id target_grasp_id, workspace_id target_workspace_id, std::vector<dual_manipulation_shared::planner_item> & path)
{
    path.clear();
    ros::Time start = ros::Time::now();
    //     std::cout<<"planning from "<<source_grasp_id<<" in workspace "<<source_workspace_id<<" to "<<target_grasp_id<<" in workspace "<<target_workspace_id<<std::endl;
    lemon::SmartDigraph::Node source, target;
    if (!getNode(source_grasp_id,source_workspace_id,source))
    {
        std::cout<<"cannot find requested source grasp (" << source_grasp_id << ") / workspace (" << source_workspace_id << ")"<<std::endl;
        return Plan();
    }
    if (!getNode(target_grasp_id,target_workspace_id,target)) 
    {
        std::cout<<"cannot find requested target grasp (" << target_grasp_id << ") / workspace (" << target_workspace_id << ")"<<std::endl;
        return Plan();
    }
    int distance;
    ros::Time before = ros::Time::now();
    Plan plan = Planner::addObject(graph.id(source),graph.id(target),20,priority);
    ros::Time after = ros::Time::now();
    std::cout<<(after-before).toSec()<<std::endl;
    for (int j=0;j < plan.nodes.size(); j++)
    {
        Node& node = plan.nodes[j];
        auto i=graph.nodeFromId(node.id);
        dual_manipulation_shared::planner_item temp;
        temp.grasp_id=grasps_ids[i];
        temp.workspace_id=grasps_positions[i];
        if(j < plan.nodes.size()-1)
            temp.departure_time=ros::Time::now()+ros::Duration((plan.nodes[j+1].start_time % UNFEASIBLE_PATH_COST)-static_arc_map[lemon::findArc(m_graph, m_graph.nodeFromId(node.id), m_graph.nodeFromId(plan.nodes[j+1].id))]);
        else
            temp.departure_time=ros::Time::now()+ros::Duration(node.start_time);
        path.push_back(temp);
        
        std::cout<< "g_id : " << temp.grasp_id << " | ws_id: " << temp.workspace_id << " | node.start_time: " << node.start_time << std::endl;
    }
    bool reached=!plan.nodes.empty();
    if (reached)
    {
        //         std::cout<<"path found"<<std::endl;
    }
    else
        std::cout<<"could not find a valid path"<<std::endl;
    ros::Time end = ros::Time::now();
    //     std::cout<<(end-start).toSec()<<std::endl;
    return plan;
}
