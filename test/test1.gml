Creator	"yFiles"
Version	"2.12"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"1"
		graphics
		[
			x	180.0
			y	270.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"2"
		graphics
		[
			x	180.0
			y	510.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"3"
		graphics
		[
			x	300.0
			y	390.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"4"
		graphics
		[
			x	480.0
			y	390.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"5"
		graphics
		[
			x	630.0
			y	390.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	0
		target	2
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	180.0
					y	270.0
				]
				point
				[
					x	180.0
					y	390.0
				]
				point
				[
					x	300.0
					y	390.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	2
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	180.0
					y	510.0
				]
				point
				[
					x	300.0
					y	510.0
				]
				point
				[
					x	300.0
					y	390.0
				]
			]
		]
	]
	edge
	[
		source	2
		target	3
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	4
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
]
